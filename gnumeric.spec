Name:             gnumeric
Epoch:            1
Version:          1.12.59
Release:          1
Summary:          Spreadsheet program for GNOME
#LGPLv2+:
#plugins/gda/plugin-gda.c
#plugins/fn-financial/sc-fin.c
#plugins/plan-perfect/charset.c
#src/widgets/gnumeric-lazy-list.h
#GPLv3+:
#src/parser.c
License:          GPL-2.0-only AND GPL-3.0-only and LGPL-2.0-or-later
URL:              http://www.gnumeric.org
Source:           https://download.gnome.org/sources/%{name}/1.12/%{name}-%{version}.tar.xz
BuildRequires: pkgconfig(glib-2.0) >= 2.40.0
BuildRequires: pkgconfig(gmodule-2.0) >= 2.40.0
BuildRequires: pkgconfig(gobject-2.0) >= 2.40.0
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 1.0.0
BuildRequires: pkgconfig(gthread-2.0) >= 2.40.0
BuildRequires: pkgconfig(gtk+-3.0) >= 3.8.7
BuildRequires: pkgconfig(libgoffice-0.10) >= 0.10.57
BuildRequires: pkgconfig(libgsf-1) >= 1.14.33
BuildRequires: pkgconfig(libxml-2.0) >= 2.4.12
BuildRequires: pkgconfig(pango) >= 1.46.0
BuildRequires: pkgconfig(pangocairo) >= 1.46.0
BuildRequires: pkgconfig(pygobject-3.0) >= 3.0.0
BuildRequires: guile-devel
BuildRequires: libappstream-glib
BuildRequires: perl-devel
BuildRequires: python3-devel
BuildRequires: bison
BuildRequires: gtk-doc
BuildRequires: intltool >= 0.35.0
BuildRequires: itstool
BuildRequires: desktop-file-utils
BuildRequires:    perl-generators
BuildRequires:    perl(ExtUtils::Embed)
BuildRequires:    perl(Getopt::Long)
BuildRequires:    perl(IO::Compress::Gzip)
BuildRequires:    psiconv-devel
BuildRequires:    zlib-devel
Requires:         hicolor-icon-theme

%description
Gnumeric is a spreadsheet program for the GNOME GUI desktop
environment.


%package devel
Summary:          Files necessary to develop gnumeric-based applications
Requires:         %{name}%{?_isa} = %{epoch}:%{version}-%{release}

%description devel
Gnumeric is a spreadsheet program for the GNOME GUI desktop
environment. The gnumeric-devel package includes files necessary to
develop gnumeric-based applications.


%package plugins-extras
Summary:          Additional plugins for Gnumeric incl. Perl and Python support
Requires:         %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:         python(abi) = %{python3_version}

%description plugins-extras
This package contains the following additional plugins for gnumeric:
* gda and gnomedb plugins:
  Database functions for retrieval of data from a database.
* perl plugin:
  This plugin allows writing of plugins in Perl.
* python-loader plugin:
  This plugin allows writing of plugins in Python.
* py-func plugin:
  Sample Python plugin providing some (useless) functions.
* gnome-glossary:
  Support for saving GNOME Glossary in .po files. 


%prep
%autosetup -p1

%build
%configure --disable-silent-rules --disable-maintainer-mode --without-gda
%disable_rpath
%make_build

%install
%make_install
%delete_la

%find_lang %{name} --all-name --with-gnome

mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install --delete-original                                  \
  --remove-category Science                                             \
  $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop

# Bytecompile Python plugins
%py_byte_compile %{__python3} $RPM_BUILD_ROOT%{_libdir}/%{name}/%{version}/plugins

%files -f %{name}.lang
%doc HACKING AUTHORS ChangeLog NEWS BUGS README
%license COPYING
%{_bindir}/*
%{_libdir}/libspreadsheet-%{version}.so
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/%{version}
%dir %{_libdir}/%{name}/%{version}/plugins
%{_libdir}/%{name}/%{version}/plugins/*/*.so
%{_libdir}/%{name}/%{version}/plugins/*/*.xml
%{_datadir}/glib-2.0/schemas/org.gnome.gnumeric.*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/metainfo/*.xml
%{_mandir}/man1/*

%files devel
%{_libdir}/libspreadsheet.so
%{_libdir}/pkgconfig/libspreadsheet-1.12.pc
%{_includedir}/libspreadsheet-1.12

%files plugins-extras
%{_libdir}/%{name}/%{version}/plugins/perl-*
%{_libdir}/%{name}/%{version}/plugins/psiconv
%{_libdir}/%{name}/%{version}/plugins/gnome-glossary
%{_libdir}/%{name}/%{version}/plugins/py-*
%{_libdir}/%{name}/%{version}/plugins/python-* 
%{_libdir}/goffice/*/plugins/gnumeric/gnumeric.so
%{_libdir}/goffice/*/plugins/gnumeric/plugin.xml

%changelog
* Sun Mar 02 2025 Funda Wang <fundawang@yeah.net> - 1:1.12.59-1
- update to 1.12.59

* Fri May 26 2023 wangtaozhi <wangtaozhi@kylinsec.com.cn> - 1:1.12.55-1
- Package init
